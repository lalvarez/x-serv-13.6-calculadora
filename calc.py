#!/usr/bin/python3

import sys

if len(sys.argv) != 4:
	sys.exit("usage error: python3 calc.py operacion operando operando")

try:
    sys.argv[2] = float(sys.argv[2])
    sys.argv[3] = float(sys.argv[3])
except ValueError:
    sys.exit("Operands must be numbers")
if sys.argv[1] == "sumar":
    result = sys.argv[2] + sys.argv[3]
elif sys.argv[1] == "restar":
    result = sys.argv[2] - sys.argv[3]
elif sys.argv[1] == "multiplicar":
    result = sys.argv[2] * sys.argv[3]
elif sys.argv[1] == "dividir":
    try:
        result = sys.argv[2]/sys.argv[3]
    except ZeroDivisionError:
        sys.exit("Can't divide by zero")
else:
	sys.exit("Can't make the operation. Try using suma resta multiplicacion or division")

print(result)
sys.exit()
